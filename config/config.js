const dotenv = require('dotenv');
dotenv.config();

module.exports	=	{
	enviroment: "development",
	development: {
		username: 'student_managment',
		password: 'jIQK4YeCRxd2BD7t',
		database: 'apptesting_studentmanagment',
		host: '95.217.58.125',
		dialect: 'mysql',
		port: '3306',
		// Use a different storage type. Default: sequelize
		migrationStorage: "json",
		// Use a different file name. Default: sequelize-meta.json
		migrationStoragePath: "sequelize_meta.json",
	},
	test: {
		dialect: "sqlite",
		storage: ":memory:",
	},
	production: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME,
		host: process.env.DB_HOSTNAME,
		port: process.env.DB_PORT,
		dialect: process.env.DB_DIALECT,
		migrationStorage: "json",

		// Use a different file name. Default: sequelize-meta.json
		migrationStoragePath: "sequelize_meta.json",
	},
	app: {
		name: 'EPLDEMO-API',
		baseUrl: 'http://localhost:5048',
	}
};
