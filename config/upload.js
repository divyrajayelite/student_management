const multer = require('multer');

var storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, 'public/images/uploads/')
    },
    filename: function(req, file, cb){
        let ext = file.originalname.substring(file.originalname.lastIndexOf('.'),file.originalname.length);
        cb(null, Date.now() + ext)
    }
});

var upload = multer({ storage: storage });

module.exports = upload;