const dotenv = require('dotenv');
dotenv.config();

const development = {
  username: 'root',
  password: 'jIQK4YeCRxd2BD7t',
  database: 'dreamerss_db',
  host: '95.217.58.125',
  dialect: 'mysql',
};

const testing = {
  username: 'root',
  password: 'jIQK4YeCRxd2BD7t',
  database: 'dreamerss_db',
  host: '95.217.58.125',
  dialect: 'mysql',
};

const production = {
  database: process.env.DB_NAME,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOSTNAME,
  dialect: process.env.DB_DIALECT,
};

module.exports = {
  development,
  testing,
  production,
};
