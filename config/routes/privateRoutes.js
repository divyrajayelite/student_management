const privateRoutes = {
    //Users
    'GET /users': 'UserController.getAll',
    'GET /user': 'UserController.get',
};

module.exports = privateRoutes;
