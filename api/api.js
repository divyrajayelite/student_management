/**
 * third party libraries
 */
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const path = require('path');
const https = require('https');
// var server = require('http').Server(app);
var multiparty = require("multiparty");
var util = require('util');
const helmet = require('helmet');
const mapRoutes = require('express-routes-mapper');
const cors = require('cors');
var multer  = require('multer')
var crypto = require("crypto");
const mime = require('mime');
const dotenv = require('dotenv');
const fs = require('fs');
dotenv.config();
/**
 * server configuration
 */
const config = require('../config/');
const dbService = require('./services/db.service');
const auth = require('./policies/auth.policy');

var options = {
  key: fs.readFileSync('./server.key'),
  cert: fs.readFileSync('./primary.crt'),
  ca: fs.readFileSync('./inter.crt')
};
// environment: development, staging, testing, production
const environment = process.env.NODE_ENV;

/**
 * express application
 */

// const server = http.Server(app);
const mappedOpenRoutes = mapRoutes(config.publicRoutes, 'api/controllers/');
const mappedAuthRoutes = mapRoutes(config.privateRoutes, 'api/controllers/');
const DB = dbService(environment, config.migrate).start();

// allow cross origin requests
// configure to only allow requests from certain origins
app.use(cors());

// secure express app
app.use(helmet({
  dnsPrefetchControl: false,
  frameguard: false,
  ieNoOpen: false,
}));

// parsing the request bodys
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/developer'), function(req, res){
    console.log(process.env.DEVELOPER);
}

// setup public dir
// app.use(express.static(path.join(__dirname, 'uploads')));
app.use(express.static('public'));
// file upload
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads/')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
    });
  }
});
var upload = multer({ storage: storage });

app.post('/upload_file', upload.single('file'), function (req, res, next) {
  try{
    return res.status(200).json({ file: res.req.file.filename });
  }catch (err) {
    console.log(err);
    return res.status(500).json({ msg: err });
  }
})


// secure your private routes with jwt authentication middleware
app.all('/private/*', (req, res, next) => auth(req, res, next));

// fill routes for express application
app.use('/public', mappedOpenRoutes);
app.use('/private', mappedAuthRoutes);
var server = https.createServer(options, app).listen(config.port,function() {
  console.log('Tes server listening on %d, in %s mode', config.port, app.get('env'));
});
// server.listen(config.port, () => {
//   if (environment !== 'production' &&
//     environment !== 'development' &&
//     environment !== 'testing'
//   ) {
//     console.error(`NODE_ENV is set to ${environment}, but only production and development are valid.`);
//     process.exit(1);
//   }
//   return DB;
// });
