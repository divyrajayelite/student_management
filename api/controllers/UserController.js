const models = require('../models/index');
const User = models.users;
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
const mailer = require('../services/mailer.service');
var nodemailer = require('nodemailer');
// const onesignal = require('../services/notification.service');
const UserController = () => {
  const register = async (req, res) => {
    const { body } = req;
    if (body.password) {
      try {
        const user = await User.create({
          firstname: body.firstname,
          lastname: body.lastname,
          password: bcryptService().generatePassword(body.password),
          mobile: body.mobile,
          rollno: body.rollno,
          dept: body.dept,
          semaster: body.semaster,
          notification_id: body.notification_id,
          status: body.status
        });
        const token = authService().issue({ id: user.id });
        // var mail = mailer().singleMail(body.email,'Welcome to Tuktuku','register',{ first_name:body.first_name,last_name:body.last_name,email:body.email })
        return res.status(200).json({ token,user });
      } catch (err) {
        console.log('=----->',err);
        return res.status(500).json({ msg: err.errors });
      }
    }
  };

  const checkEmail = async (req, res) =>{
    const { body } = req;
    try {
      const email = await User.findAll({
        where: { email: body.email },
      })
      if(email.length < 1){
        return res.status(404).json({ status: "Email Not Exist" });
      }else{
        return res.status(200).json({ status: "Email Exist" });
      }
    }catch(err){
      return res.status(500).json({ err });
    }
  }

  const login = async (req, res) =>{
    const { email, password } = req.body;

    if (email && password) {
      try {
        const user = await User
            .findOne({
              where: {
                email,
              },
            });

        if (!user) {
          return res.status(400).json({ msg: 'Bad Request: User not found' });
        }

        if (bcryptService().comparePassword(password, user.password)) {
          const token = authService().issue({ id: user.id });

          return res.status(200).json({ token, user });
        }

        return res.status(401).json({ msg: 'Unauthorized' });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }

    return res.status(400).json({ msg: 'Bad Request: Email or password is wrong' });
  }

  const getAll = async (req, res) => {
    try {
      const usrs = await User.findAll({
        include: [{ all: true, nested: true }],
      });
      return res.status(200).json({ usrs });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  }

  const get = async (req, res) => {
    try {
      const usrs = await User.find({
        where: { id: req.param('id') },
        include: [{ all: true, nested: true }],
      });
      return res.status(200).json({ usrs });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  }


  return {
    register,
    checkEmail,
    login,
    getAll,
    get
  };
};
module.exports = UserController;
