
module.exports = function(sequelize, DataTypes) {
  const users = sequelize.define('users', {
        id: {
          autoIncrement: true,
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true
        },
        firstname: {
          type: DataTypes.STRING(50),
          allowNull: true
        },
        lastname: {
          type: DataTypes.STRING(50),
          allowNull: true
        },
        email: {
          type: DataTypes.TEXT,
          allowNull: true
        },
        password: {
          type: DataTypes.TEXT,
          allowNull: true
        },
        mobile: {
          type: DataTypes.STRING(10),
          allowNull: true
        },
        rollno: {
          type: DataTypes.INTEGER(10),
          allowNull: true
        },
        dept: {
          type: DataTypes.STRING(50),
          allowNull: true
        },
        semaster: {
          type: DataTypes.STRING(50),
          allowNull: true
        },
        notification_id: {
          type: DataTypes.TEXT,
          allowNull: true
        },
        user_type_id: {
          type: DataTypes.INTEGER(11),
          allowNull: true
        },
        status: {
          type: DataTypes.INTEGER(4),
          allowNull: true
        }
      },
      {
        sequelize,
        tableName: 'users',
        timestamps: false,
      },

  );

  return users;
};
