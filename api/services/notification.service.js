var OneSignal = require('onesignal-node');

const sendNoti = () => {

	const sendGeneralNotification = ( msg ) =>{
		const myClient = new OneSignal.Client({
			userAuthKey: '',
			app: { appAuthKey: '', appId: '' }
		});
		const firstNotification = new OneSignal.Notification({
			contents: {
				en: msg,
			},
			included_segments: ["Active Users", "Inactive Users"]
		});
		return myClient.sendNotification(firstNotification);
	}

	const sendDeviceSpecificNotification = ( token, msg ) => {
		const myClient = new OneSignal.Client({
			userAuthKey: '',
			app: { appAuthKey: '', appId: '' }
		});
		const firstNotification = new OneSignal.Notification({
			contents: {
				en: msg,
			},
			include_player_ids: [token]
		});
		return myClient.sendNotification(firstNotification);
	}

	return {
		sendGeneralNotification,
		sendDeviceSpecificNotification
	};
  };

module.exports = sendNoti;